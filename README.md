<n>RESUMEN</n>

Este es un script para eliminar la versión snap del navegador firefox en Ubuntu 22.04 LTS e instalar la versión tradicional en formato deb atraves del PPA de Mozilla Security.

<n>INSTRUCCIONES DE EJECUCIÓN</n>

chmox +x redfox.sh
./redfox.sh
